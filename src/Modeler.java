import java.util.ArrayList;

// Searching for weights in problem
// w1*num of cylinders + w2*number of displacement + w3*number of horsepower ...

public class Modeler {
    private final double[] STEP_SIZES = new double[]{
        0.00001,
        0.00001, // Conv=.0005 Cylinders Average error of around 16.4% (small range)
        0.0000001, // Conv=.000005 Displacement Average error of around 59.9%
        0.0000001, // Conv=.000005 Horsepower Average error of around 18.1%
        0.000000001, // Conv=.000005 Acceleration Average error of around 26.0%
        0.000000000001, // Conv=.000000005 Weight Average error of around 50.3% (very big range)
        0.000000000001 // Conv=.0000000005 Year Average error of around 31.8% (big range)
    };

    private final double[] CONVERGENCE_THRESHOLDS = new double[]{
        0.0005,
        0.0005, // Cylinders
        0.000005, // Displacement
        0.000005, // Horsepower
        0.000005, // Acceleration
        0.000000005, // Weight
        0.0000000005 // Year
    };

    private final double CONVERGENCE_THRESHOLD = .005;//sumOfDoubleArray(CONVERGENCE_THRESHOLDS); Average error of aroud 26.5%

    public ArrayList<Double> dataWeights;

    private ArrayList<Double> weightDiffs;

    private static double sumOfDoubleArray(double[] array) {
        double returnValue = 0;

        for(double number : array) {
            returnValue += number;
        }

        return returnValue;
    }

    public Modeler() {
        dataWeights = new ArrayList<>();
        weightDiffs = new ArrayList<>();

        for(int i = 0; Car.Attribute.count > i; ++i) {
            dataWeights.add(1.0);
        }

        for(int i = 0; Car.Attribute.count > i; ++i) {
            weightDiffs.add(1.0);
        }
    }

    private boolean converged(int i) {
        return (Math.abs(weightDiffs.get(0)) + Math.abs(weightDiffs.get(i))) < CONVERGENCE_THRESHOLDS[i];
    }

    private boolean completeConvergence() {
        double sum = 0;

        for(int i = 0; weightDiffs.size() - 1 > i; ++i) {
            /*if(!converged(i)) {
                return false;
            }*/
            sum += Math.abs(weightDiffs.get(i));
        }
        //return true;

        return sum < CONVERGENCE_THRESHOLD;
    }

    private double expectedMpg(Vector attributes) {
        double returnValue = dataWeights.get(0);

        for(int i = 1; attributes.length() - 1 > i; ++i) {
            returnValue += (attributes.get(i + 1) * dataWeights.get(i + 1));
        }

        return returnValue;
    }

    public void generateWeights(Iterable<Car> cars) {
        // convergence - when the value of the weights doesn't vary by much from one iteration to the next
        // Cylinder, displacement, and horsepower might be useful
        // for each weight: past weight value + Step Size * ( sum of ( the related car values * ( mpg - expected mpg))
        // all vectors start with a one

        while (!completeConvergence()) {
            for(int i = 0; Car.Attribute.count > i; ++i) {
                //if(!converged(i)) {
                    double sumOfErrors = 0;

                    for(Car car : cars) {
                        if(car.acceleration() != Car.NA && car.weight() != Car.NA &&
                            car.cylinders() != Car.NA && car.mpg() != Car.NA && car.horsepower() != Car.NA &&
                            car.displacement() != Car.NA && car.year() != Car.NA) {
                            Vector attributes = car.getAttributes();

                            sumOfErrors += attributes.get(i) * (car.mpg() - expectedMpg(attributes));
                        }
                    }

                    dataWeights.set(i, dataWeights.get(i) + (STEP_SIZES[i] * sumOfErrors));
                    weightDiffs.set(i, STEP_SIZES[i] * sumOfErrors);
                    if (!dataWeights.get(i).equals(Double.NaN)) {
                        System.out.println(i + " = " + dataWeights.get(i));
                    }
                /*} else {
                    if(i != 0) {
                        System.out.println("FLAG");
                    }
                }*/
            }
        }
        System.out.println("finished");
    }

    public void printErrors(Iterable<Car> cars) {
        int carNum = 0;
        double errorSum = 0;

        for(Car car : cars) {
            double error = (Math.abs(expectedMpg(car.getAttributes()) - car.mpg()) / car.mpg()) * 100;

            System.out.println(error);
            //System.out.println(expectedMpg(car.getAttributes()) + ":" + car.mpg());

            ++carNum;
            errorSum += error;
        }

        System.out.println("Average error = " + errorSum / carNum);
    }
}
