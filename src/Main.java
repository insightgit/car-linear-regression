public class Main {

    public static void main(String[] args) {
	    Modeler modeler = new Modeler();

	    modeler.generateWeights(Car.trainingCars);

	    modeler.printErrors(Car.testingCars);
    }
}
